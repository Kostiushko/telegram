// sendCode = function () {
//     let i = new XMLHttpRequest();
//     i.onreadystatechange = function () {
//         if (this.readyState === 4 && this.status === 200) {
//             let r = JSON.parse(i.responseText);
//             if (r['result'] === true)
//                 document.getElementById('tg-code').style.display = '';
//             document.getElementById('tg-button').onclick = function () {
//                 let n = new XMLHttpRequest();
//                 n.onreadystatechange = function () {
//                     if (this.readyState === 4 && this.status === 200) {
//                         let e = JSON.parse(n.responseText);
//                         if (e['result'] === true) {
//                             window.location.href = e['redirect_url']
//                         } else {
//                             // TODO: Wrong code! Handle it!
//                             // TODO: or wrong request
//                         }
//                     }
//                 };
//                 n.open('POST', document.getElementById('tg-form').getAttribute('action'));
//                 n.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
//                 n.send(JSON.stringify({'code': document.getElementById('tg-code').value}));
//             }
//         }
//     };
//     i.open('POST', document.getElementById('tg-form').getAttribute('action'));
//     i.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
//     i.send(JSON.stringify({'phone': document.getElementById('tg-phone').value}));
// };


const chat_data = {
  chat_id: number = null,
  account_id: number = null,
  group_name: string = null
}


function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    let cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
      let cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

const csrf_token = getCookie('csrftoken');


$(document).ready(function () {
  // $('#tg-phone').attr('value', '+380935531336');
  $('#tg-button').click(function () {
    $.ajaxSetup({
      beforeSend: function (xhr, settings) {
        xhr.setRequestHeader("X-CSRFToken", csrf_token);
      }
    });
    $.post((document.location.origin + $('#tg-form').attr('action')), {
      phone: $('#tg-phone').val(),
    }, function (json) {
      console.log(json);
      if (json.result === true) {
        $('#tg-code').css('display', '');
        $('#tg-button').unbind('click').click(function () {
          $.ajaxSetup({
            beforeSend: function (xhr, settings) {
              xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
          });
          $.post((document.location.origin + $('#tg-form').attr('action')), {
            code: $('#tg-code').val(),
          }, function (json) {
            if (json.result === true) {
              $(location).attr('href', json.redirect_to);
            } else {
              $('#tg-code').addClass('form-control-danger');
            }
          }, 'json')
          // $.getJSON($('#tg-form').attr('action'), {'phone': $('#tg-phone').attr('value')}, function (json) {
          //     if (json.result === true) {
          //         $(location).attr('href', json.redirect_to);
          //     }
          // })
        })
      } else {
        $('#tg-phone').addClass('form-control-danger');
      }
    }, 'json')
  })
});

$('#tg-chat-search').keyup(function () {
  chat_data.account_id = getQueryParams().account_id;
  if (this.value.length >= 4 && chat_data.account_id) {
    $.ajaxSetup({
      beforeSend: function (xhr, settings) {
        xhr.setRequestHeader("X-CSRFToken", csrf_token);
      }
    });
    $.post(
      (document.location.origin + $('#tg-chat-search-form').attr('action')),
      {search_query: this.value, account_id: chat_data.account_id},
      function (json) {
        $('#tg-search-chat-result').html(
          // '<a href="/app/contacts/add/1" type="button" class="btn btn-outline-primary" style="margin:15px 0">Export to csv</a>\n'
        );
        $('#tg-search-chat-result').empty();

        let chats = json.chats;
        $.each(chats, function (index, chat) {
          console.log(index, chat)
          if (index < 5) {
            $('#tg-search-chat-result').append(
              `<li>
              <div class="row">
              <div class="col-md-6 col-4 description">
              <h5>${chat.title}<br><small><b>@</b>${chat.username}</small></h5>
              </div>
              <div class="col-md-2 col-2">
              <button id="tg-chat-button-add${chat.id}" tg-chat-name="${chat.title}" type="button" rel="tooltip" title="Add this chat"
              onclick="addChat(${chat.id},'${chat._}', '${chat.username}')" class="btn btn-just-icon btn-round btn-outline-danger btn-tooltip" 
              ><i class="fa fa-plus"></i></button>
              </div>
              </div>
              </li>`
            );
          } else {
            return false;
          }
        });
      }, 'json')
  }
});

function addChat(id, type, username) {
  console.log(id, type);
  let chat_name = 'Some chat';

  try {
    chat_name = $(`#tg-chat-button-add${id}`).attr('tg-chat-name')
  } catch (e) {
    console.error(e)
  }

  $.ajaxSetup({
    beforeSend: function (xhr, settings) {
      xhr.setRequestHeader("X-CSRFToken", csrf_token);
    }
  });
  const data = {
    chat_id: id,
    chat_name: chat_name,
    account_id: chat_data.account_id,
    type: type
  }
  if (type === 'Channel') {
    data.chat_name = username;
  }
  $.post((document.location.origin + $('#tg-chat-search-form').attr('add-chat-action')), data,
    function (json) {
      if (json.result === true) {
        $(location).attr('href', json.redirect_to);
      } else {
        confirm(json.error, 'Error!')
      }
    }, 'json')
}

function getQueryParams() {
  const params = {};
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    params[hash[0]] = hash[1];
  }
  return params;
}

$(document).ready(function () {
  $('input:radio[name=account]:checked').change(function () {
    if ($("input[name='bedStatus']:checked").val() == 'allot') {
      alert("Allot Thai Gayo Bhai");
    }
    if ($("input[name='bedStatus']:checked").val() == 'transfer') {
      alert("Transfer Thai Gayo");
    }
  });
});

$('.card-input-element').each(function (element) {
  if (this.checked === true) {
    setChatData(this.name, this.value, this.id)
  }
})

$('.card-input-element').change(function (event) {
  setChatData(this.name, this.value, this.id);
});

function setChatData(name, value, id) {
  switch (name) {
    case 'account':
      chat_data.account_id = id;
      break;
    case 'contact_group':
      chat_data.chat_id = value;
      break;
  }

  console.log(chat_data)
}


$('input.form-control:text[name=group_name]').change(function () {
  chat_data.group_name = this.value;
});


$('#create-group-button').click(function (event) {
  if (checkChatData()) {
    $('#exampleModal').modal('show');
  }
});


$('#modal-group-form').on('submit', function (event) {
  $.ajaxSetup({
    beforeSend: function (xhr, settings) {
      xhr.setRequestHeader("X-CSRFToken", csrf_token);
    }
  });

  const url = document.location.origin + '/app/';
  console.log(url);
  $('#modal-group-form .btn.with-loader').addClass('loading');
  $.post((url), chat_data, function (data) {
    console.log(data);
    $('#modal-group-form .btn.with-loader').removeClass('loading');
    location.reload()
  });
  event.preventDefault();
});

$('a.import-contacts').click(function (event) {
  if (checkChatData()) {
    const url = `${document.location.origin}/app/add_contacts_to/${event.target.id}`;
    $.ajaxSetup({
      beforeSend: function (xhr, settings) {
        xhr.setRequestHeader("X-CSRFToken", csrf_token);
      }
    });
    $(this).addClass('loading');
    $.post((url), chat_data, function (data) {
      console.log(data);
      $(this).removeClass('loading');
      location.reload();
      alert('Successfully imported contacts');
    });
  }
})

$('#af-telegram-group a').click(function (event) {
  event.preventDefault();
  if (!chat_data.account_id) {
    scrollTo('accounts');
  } else {
    window.location.href = event.currentTarget.href + '?account_id=' + chat_data.account_id;
  }
})

function scrollTo(id) {
  document.getElementById(id).scrollIntoView({behavior: 'smooth'});
}

function checkChatData() {
  if (!chat_data.account_id) {
    scrollTo('accounts');
    return false;
  } else if (!chat_data.chat_id) {
    scrollTo('contact_groups');
    return false;
  }
  return true;
}