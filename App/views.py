import json
import logging
from django.shortcuts import get_object_or_404
from telethon.errors.rpcerrorlist import ChatAdminRequiredError, UserPrivacyRestrictedError
from django.db.utils import IntegrityError
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django.contrib import messages
from django.utils.datastructures import MultiValueDictKeyError
from telethon.tl.functions.users import GetFullUserRequest, GetUsersRequest
from App.models import Message, Account, ContactGroup, CreatedGroup
from Backend.connector import Connector
from .forms import CreatedGroupForm, ImportContactForm
import pandas
from django.http import HttpResponse
import random
from django.db.models import F
from telethon.tl.functions.contacts import ResolveUsernameRequest
from telethon.tl.functions.messages import CreateChatRequest, EditChatAdminRequest, AddChatUserRequest
from telethon.tl.functions.channels import CreateChannelRequest, InviteToChannelRequest
from telethon.tl.types import PeerChannel, PeerChat, PeerUser, UserFull
from telethon.tl.functions.messages import AddChatUserRequest
from telethon.tl.functions.contacts import SearchRequest
import asyncio
from telethon.tl.functions.channels import CreateChannelRequest
from telethon.tl.functions.channels import InviteToChannelRequest
from telethon.tl.functions.channels import EditAdminRequest
from telethon.tl.types import ChatAdminRights
import time
df = None
connector = None

def th_render(request):
    return render(request, 'App/thing.html')


def app_home(request, to_csv=None):
    if to_csv:
        accounts = Account.objects.all()
        df = pandas.DataFrame({'id': [account.id for account in accounts],
                               'number': [account.phone_number for account in accounts],
                               'time_created': [account.time_created for account in accounts],
                               'session': [account.session for account in accounts],
                               })
        response = HttpResponse(df.to_csv(encoding='utf-8'), content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="accounts.csv"'
        return response

    if request.method == "POST":
        import_form = ImportContactForm(request.POST, request.FILES)
        form = CreatedGroupForm(request.POST)
        try:
            post_data = request.POST
            co_connector = Connector(session=Account.objects.get(id=post_data['account_id']).session)
            contacts = ContactGroup.objects.get(chat_id=int(post_data['chat_id']))
            users = [json.loads(user) for user in json.loads(contacts.chat_members_list_as_json)]
            # users_username = [user['username'] for user in users if user['username'] != None]
            # users_id_with_username = [user['id'] for user in users if user['username'] != None]
            users_id_with_username = []
            count = 0
            for user in users:
                if user['username'] != None:
                    try:
                        if co_connector.client.loop.run_until_complete(co_connector.client.get_entity(user['username'])):
                            count += 1
                            users_id_with_username.append(user['id'])
                            if count == 5:
                                count = 0
                                time.sleep(1)
                    except ValueError:
                        continue
                else:
                    try:
                        if co_connector.client.loop.run_until_complete(co_connector.client.get_entity(user['id'])):
                            count += 1
                            users_id_with_username.append(user['id'])
                            if count == 5:
                                count = 0
                                time.sleep(1)
                    except ValueError:
                        continue


            all_account_telegram_id = [account.telegram_account_id for account in
                                       Account.objects.all().exclude(id=post_data['account_id'])]

            all_account_telegram_username = [account.telegram_username for account in
                                             Account.objects.all().exclude(id=post_data['account_id'])
                                             if account.telegram_username != None]

            for username in all_account_telegram_username:
                co_connector.client.loop.run_until_complete(co_connector.client.get_entity(username))

            for i in all_account_telegram_id:
                if i not in users_id_with_username:
                    users_id_with_username.append(i)

            create_chat = co_connector.client.loop.run_until_complete(co_connector.client(CreateChatRequest(
                title=post_data['group_name'], users=users_id_with_username)))

            # telegram_chat_id = co_connector.client.loop.run_until_complete(
            #     co_connector.client.get_entity(post_data['group_name'])).id
            telegram_chat_id = create_chat.chats[0].id

            for account_id in all_account_telegram_id:
                co_connector.client.loop.run_until_complete(co_connector.client(EditChatAdminRequest(
                    chat_id=telegram_chat_id,
                    user_id=account_id,
                    is_admin=True
                )))

            if form.is_valid():
                new_group = form.save()
                new_group.users_added_count = len(users_id_with_username)
                new_group.users_to_add_count = len(users_id_with_username)
                new_group.chat_id = telegram_chat_id
                new_group.save()

        except MultiValueDictKeyError:
            pass
        if import_form.is_valid():
            try:
                file = request.FILES['file']
                csv = pandas.read_csv(file, encoding='utf-8', sep=',')
                df = pandas.DataFrame(csv, columns=['id', 'first_name', 'last_name', 'username'])
                data = [row[1].to_json() for row in df.iterrows()]
                contact_group = ContactGroup.objects.create(
                    chat_id=random.randint(1, 10000),
                    chat_name=file.name.split(".")[0],
                    created_by_telegram_user_id=5,
                    users_count=len(data) + 1,
                    chat_members_list_as_json=json.dumps(data)
                )
                contact_group.save()
            except MultiValueDictKeyError:
                pass

    return render(request, 'App/home.html', {'notification': 'Message created successfully',
                                             'accounts': Account.objects.all(),
                                             'contact_groups': ContactGroup.objects.all(),
                                             'messages_': Message.objects.all(),
                                             'created_groups': CreatedGroup.objects.all(),
                                             'form': CreatedGroupForm(),
                                             'import_form': ImportContactForm()
                                             })


def add_users_to_group(request, chat_id=None):

    if request.method == "POST":
        post_data = request.POST
        co_connector = Connector(session=Account.objects.get(id=post_data['account_id']).session)
        contact_group = get_object_or_404(ContactGroup, chat_id=post_data['chat_id'])
        created_group = get_object_or_404(CreatedGroup, chat_id=chat_id)
        users = [json.loads(user) for user in json.loads(contact_group.chat_members_list_as_json)]

        users_id_with_username = []
        count = 0
        for user in users:
            if user['username'] != None:
                try:
                    co_connector.client.loop.run_until_complete(co_connector.client.get_entity(user['username']))
                    count += 1
                    users_id_with_username.append(user['id'])
                    if count == 5:
                        count = 0
                        time.sleep(1)
                except ValueError:
                    continue
            else:
                try:
                    co_connector.client.loop.run_until_complete(co_connector.client.get_entity(user['id']))
                    count += 1
                    users_id_with_username.append(user['id'])
                    if count == 5:
                        count = 0
                        time.sleep(1)
                except ValueError:
                    continue

        # users_id_in_contact_group = [user['id'] for user in users]
        users_id_in_telegram_account = [user.id for user in co_connector.client.loop.run_until_complete(
            co_connector.client.get_participants(PeerChat(chat_id=int(chat_id))))]
        count = 0
        for user_id in users_id_with_username:
            if user_id not in users_id_in_telegram_account:
                try:
                    co_connector.client.loop.run_until_complete(co_connector.client(AddChatUserRequest(
                        chat_id=created_group.chat_id,
                        user_id=user_id,
                        fwd_limit=10000
                    )))
                    count += 1
                    if count == 1:
                        count = 0
                        time.sleep(5)
                except UserPrivacyRestrictedError:
                    continue
        created_group.users_added_count = F('users_added_count') + count
        created_group.users_to_add_count = F('users_to_add_count') + count
        created_group.save()
        return HttpResponse('Ok')


def delete_created_group(request, group_id):
    group = CreatedGroup.objects.get(id=group_id)
    group.delete()
    return redirect('App-home')


def add_account(request):
    if request.method == 'POST':
        pass
    return render(request, 'App/Accounts/register.html')


def add_contacts(request, export=None):
    if export:
        response = HttpResponse(df.to_csv(encoding='utf-8', sep=",", index=False), content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="chats.csv"'
        return response

    return render(request, 'App/Contacts/add_contacts.html')


def add_message(request):
    if request.method == 'POST':
        post_message = request.POST
        # Our message
        message = Message(name=post_message['name'], content=post_message['content'])
        message.save()
        # Django message
        # messages.success(request, f'Message {message.name} created')  # TODO: make this
        return redirect('App-home')
    else:
        return render(request, 'App/Messages/add_message.html')


def manage_contacts_group(request, chat_id: int = None, to_csv=None):
    if not chat_id:
        return redirect('App-home')
    contact_group = ContactGroup.objects.filter(chat_id=chat_id).first()
    if to_csv:
        users = [json.loads(user) for user in json.loads(contact_group.chat_members_list_as_json)]
        df = pandas.DataFrame({'id': [user['id'] for user in users],
                               'first_name': [user['first_name'] for user in users],
                               'last_name': [user['last_name'] for user in users],
                               'username': [user['username'] for user in users],
                               })
        response = HttpResponse(df.to_csv(sep=',', encoding='utf-8', index=False), content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(contact_group.chat_name)

        return response
    return render(request, 'App/Contacts/manage_contacts_group.html', {
        'title': 'Manage Group',
        'contacts_group': contact_group,
        'users': [json.loads(user) for user in json.loads(
            contact_group.chat_members_list_as_json
        )] if contact_group is not None else None})


def delete_account(request, phone_number: int = None):
    if not phone_number:
        return HttpResponseBadRequest()
    account = Account.objects.filter(phone_number=phone_number).first()
    if account:
        account.delete()
    return redirect('App-home')


def delete_contact_group(request, chat_id: int = None):
    if not chat_id:
        return HttpResponseBadRequest()
    contact_group = ContactGroup.objects.filter(chat_id=chat_id).first()
    if contact_group:
        contact_group.delete()
    return redirect('App-home')


def delete_message(request, message_name: str = None):
    if not message_name:
        return HttpResponseBadRequest()
    message = Message.objects.filter(name=message_name).first()
    if message:
        message.delete()
    return redirect('App-home')


def api_account_add(request):
    if request.method == 'POST':
        global connector
        # post = json.loads(request.POST['data'])
        # post = json.loads(request.POST)
        post = request.POST  # Fuck, I don't even need json, it's already formatted. Amazing Django!!
        # Let's make path to session file equal to phone number: str
        try:
            if post['phone']:
                connector = Connector('', phone_number=post['phone'])
                # TODO: write this in a correct way!!!
                result = connector.client.loop.run_until_complete(connector.start(post['phone']))
                return HttpResponse(json.dumps({'result': result}))
        except MultiValueDictKeyError:
            try:
                if post['code']:
                    if connector is not None:
                        # TODO: write this in a right way
                        result = connector.client.loop.run_until_complete(connector.sign_in(post['code']))

                        account = Account.objects.create(phone_number=connector.phone_number, first_name='',
                                                         last_name='',
                                                         session=connector.session.save(),
                                                         telegram_account_id=connector.client.loop.run_until_complete(
                                                             connector.client.get_me()).id,
                                                         telegram_username=connector.client.loop.run_until_complete(
                                                             connector.client.get_me()
                                                         ).username
                                                         )  # TODO: Add here more data
                        account.save()
                        return HttpResponse(json.dumps({'result': result, 'redirect_to': '/app/'}))
                    else:
                        return HttpResponse(json.dumps({'result': False,
                                                        'error': 'Connector is not running at the moment'}))
            except MultiValueDictKeyError:
                pass
        return HttpResponseBadRequest()
    else:
        return redirect('App-home')


def api_get_chats(request):
    # Takes:
    # - session: str
    # - search_query: str
    global df
    if request.method == 'POST':
        try:
            post = request.POST
            co_connector = Connector(session=Account.objects.get(id=request.POST['account_id']).session)
            try:
                limit = post['limit']
            except MultiValueDictKeyError:
                limit = 5
            if co_connector.client.loop.run_until_complete(co_connector.start()):
                chats = co_connector.client.loop.run_until_complete(co_connector.search_chats(
                    query=post['search_query'],
                    limit=limit,
                ))

                from_json = json.loads(chats)
                df = pandas.DataFrame({'id': [chat['id'] for chat in from_json['chats']],
                                       'title': [chat['title'] for chat in from_json['chats']],
                                       'date': [chat['date'] for chat in from_json['chats']],
                                       # 'username': [chat['username'] for chat in from_json['chats']],
                                       })
                return HttpResponse(chats)


        except MultiValueDictKeyError:
            pass
        return HttpResponseBadRequest()

    else:
        return redirect('App-home')


def api_add_chat(request):
    if request.method == 'POST':
        try:

            chat_id = request.POST['chat_id']
            chat_name = request.POST['chat_name']
            account_id = request.POST['account_id']
            dialog_type = request.POST['type']
            if chat_id.isdigit():
                co_connector = Connector(session=Account.objects.get(id=account_id).session)
                if co_connector.client.loop.run_until_complete(co_connector.client.is_user_authorized()):
                    # co_connector.client.loop.run_until_complete(co_connector.client.get_entity(chat_name))
                    try:
                        if dialog_type == "Channel":
                            participants_as_json, users_count = co_connector.client.loop.run_until_complete(
                                co_connector.get_participants(int(chat_id), chat_name)
                            )
                        else:
                            participants_as_json, users_count = co_connector.client.loop.run_until_complete(
                                co_connector.get_participants_by_chat(int(chat_id), chat_name)
                            )
                        contact_group = ContactGroup(chat_id=chat_id, chat_name=chat_name,
                                                     users_count=users_count,
                                                     created_by_telegram_user_id=co_connector.client.loop.run_until_complete(
                                                         co_connector.client.get_me()
                                                     ).id,
                                                     chat_members_list_as_json=participants_as_json)
                        contact_group.save()
                        return HttpResponse(json.dumps({'result': True,
                                                        'redirect_to': '/app/'}))

                    except ChatAdminRequiredError:
                        return HttpResponse(json.dumps({'error': "You are not admin of this channel"}))
                    except IntegrityError:
                        return HttpResponse(json.dumps({'error': 'This channel is already added'}))
            else:
                return HttpResponseBadRequest()

        except MultiValueDictKeyError:
            return HttpResponseBadRequest()
    else:
        return redirect('App-home')


def api_send_message(request):
    if request.method == 'POST':
        try:
            data = request.POST
            phone_number = data['phone_number']
            contact_group_id = data['contact_group_id']
            message = data['message_text']

            session_string = Account.objects.filter(phone_number=phone_number).first().session
            contact_group = ContactGroup.objects.filter(
                chat_id=contact_group_id).first()
            chat_name = contact_group.chat_name
            # chat_members_list = json.loads(contact_group.chat_members_list_as_json)
            if session_string and message:
                # recipients = []
                # for chat_member in chat_members_list:
                #     recipients.append(json.loads(chat_member))
                co_connector = Connector(session=session_string)
                result = co_connector.client.loop.run_until_complete(
                    co_connector.send_messages(chat_name, contact_group_id, message))
                return HttpResponse(json.dumps({'result': result,
                                                'redirect_to': '/app/'}))
            else:
                return HttpResponseBadRequest()

        except MultiValueDictKeyError:
            return HttpResponseBadRequest()
    else:
        return redirect('App-home')


def api_add_users_to_group(request):
    if request.method == 'POST':
        try:
            data = request.POST
            phone_number = data['phone_number']
            contact_group_id = data['contact_group_id']
            group_name = data['group_name']

            try:
                to_group_id = data['to_group_id']
            except KeyError:
                to_group_id = None

            session_string = Account.objects.filter(phone_number=phone_number).first().session
            contact_group = ContactGroup.objects.filter(
                chat_id=contact_group_id).first()
            chat_name = contact_group.chat_name
            # chat_members_list = json.loads(contact_group.chat_members_list_as_json)
            if session_string and group_name:
                # recipients = []
                # for chat_member in chat_members_list:
                #     recipients.append(json.loads(chat_member))
                if not to_group_id:
                    created_group = CreatedGroup(group_name=group_name)
                    created_group.save()
                    to_group_id = created_group.id

                co_connector = Connector(session=session_string)
                try:
                    result, added_count = co_connector.client.loop.run_until_complete(
                        co_connector.add_users_to_group(chat_name, contact_group_id, group_name, to_group_id))
                    contact_group.added_users_count += added_count
                    contact_group.save()
                except Exception as e:
                    logging.error(f'Error: {e.__class__}\n'
                                  f'Message: {e}')
                    result = False
                    added_count = 0
                return HttpResponse(json.dumps({'result': result,
                                                'added_count': added_count,
                                                'redirect_to': '/app/'}))
            else:
                return HttpResponseBadRequest()
        except MultiValueDictKeyError:
            return HttpResponseBadRequest()
    else:
        return redirect('App-home')
