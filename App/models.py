from django.db import models
from django.utils import timezone


class Message(models.Model):
    name = models.CharField(max_length=30, unique=True)
    content = models.TextField()
    time_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class Account(models.Model):
    phone_number = models.BigIntegerField(unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    session = models.TextField(unique=True)
    telegram_account_id = models.BigIntegerField(unique=True)
    telegram_username = models.CharField(max_length=32, blank=True, null=True)
    time_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.phone_number)  # TODO: return Name


class ContactGroup(models.Model):
    chat_id = models.IntegerField(unique=True)
    chat_name = models.CharField(max_length=30)
    users_count = models.IntegerField()
    added_users_count = models.IntegerField(default=0)
    chat_members_list_as_json = models.TextField(default='')
    created_by_telegram_user_id = models.IntegerField()
    time_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.chat_name


class CreatedGroup(models.Model):
    group_name = models.CharField(max_length=50)
    users_to_add_count = models.IntegerField(default=0)
    users_added_count = models.IntegerField(default=0)
    chat_id = models.BigIntegerField(unique=True, blank=True, null=True)

    def __str__(self):
        return self.group_name
