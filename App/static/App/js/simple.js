// sendCode = function () {
//     let i = new XMLHttpRequest();
//     i.onreadystatechange = function () {
//         if (this.readyState === 4 && this.status === 200) {
//             let r = JSON.parse(i.responseText);
//             if (r['result'] === true)
//                 document.getElementById('tg-code').style.display = '';
//             document.getElementById('tg-button').onclick = function () {
//                 let n = new XMLHttpRequest();
//                 n.onreadystatechange = function () {
//                     if (this.readyState === 4 && this.status === 200) {
//                         let e = JSON.parse(n.responseText);
//                         if (e['result'] === true) {
//                             window.location.href = e['redirect_url']
//                         } else {
//                             // TODO: Wrong code! Handle it!
//                             // TODO: or wrong request
//                         }
//                     }
//                 };
//                 n.open('POST', document.getElementById('tg-form').getAttribute('action'));
//                 n.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
//                 n.send(JSON.stringify({'code': document.getElementById('tg-code').value}));
//             }
//         }
//     };
//     i.open('POST', document.getElementById('tg-form').getAttribute('action'));
//     i.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
//     i.send(JSON.stringify({'phone': document.getElementById('tg-phone').value}));
// };


function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const csrf_token = getCookie('csrftoken');


$(document).ready(function () {
    // $('#tg-phone').attr('value', '+380935531336');
    $('#tg-button').click(function () {
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        });
        $.post((document.location.origin + $('#tg-form').attr('action')), {
            phone: $('#tg-phone').val(),
        }, function (json) {
            console.log(json);
            if (json.result === true) {
                $('#tg-code').css('display', '');
                $('#tg-button').unbind('click').click(function () {
                    $.ajaxSetup({
                        beforeSend: function (xhr, settings) {
                            xhr.setRequestHeader("X-CSRFToken", csrf_token);
                        }
                    });
                    $.post((document.location.origin + $('#tg-form').attr('action')), {
                        code: $('#tg-code').val(),
                    }, function (json) {
                        if (json.result === true) {
                            $(location).attr('href', json.redirect_to);
                        } else {
                            $('#tg-code').addClass('form-control-danger');
                        }
                    }, 'json')
                    // $.getJSON($('#tg-form').attr('action'), {'phone': $('#tg-phone').attr('value')}, function (json) {
                    //     if (json.result === true) {
                    //         $(location).attr('href', json.redirect_to);
                    //     }
                    // })
                })
            } else {
                $('#tg-phone').addClass('form-control-danger');
            }
        }, 'json')
    })
});

$('#tg-chat-search').keyup(function () {
    if (this.value.length >= 4) {
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        });
        $.post(
          (document.location.origin + $('#tg-chat-search-form').attr('action')),

            function (json) {
                $('#tg-search-chat-result').html('');
                let chats = json.chats;
                $.each(chats, function (index, chat) {
                    // console.log(index.toString(), '      ', chat.toString());
                    if (index < 5) {
                        alert(1);
                        // console.log('Good');
                        $('#tg-search-chat-result').append('<li>\n' +
                            '                                    <div class="row">\n' +
                            // TODO: create image place
                            // '                                        <div class="col-md-2 col-3">\n' +
                            // '                                            <img src="blob:' + chat.logo_url + '"\n' +
                            // '                                                 alt="Circle Image" class="img-circle img-no-padding img-responsive">\n' +
                            // '                                        </div>\n' +
                            '                                        <div class="col-md-6 col-4 description">\n' +
                            '                                            <h5>' + chat.title + '<br>\n' +
                            '                                                <small><b>@</b>' + chat.username + '</small></h5>\n' +
                            '                                        </div>\n' +
                            '                                        <div class="col-md-2 col-2">\n' +
                            '                                            <button id="tg-chat-button-add' + chat.id + '" ' +
                            'tg-chat-name="' + chat.title + '" type="button" onclick="addChat(' + chat.id + ')" class="btn btn-just-icon btn-round btn-outline-danger btn-tooltip"\n' +
                            '                                                    rel="tooltip" title="Add this chat"><i class="fa fa-plus"></i></button>\n' +
                            '                                        </div>\n' +
                            '                                    </div>\n' +
                            '                                </li>');
                    } else {
                        return false;
                    }
                });
                $('#tg-search-chat-result').append('<script>alert("123");</script>');
            },
          'json'
        )
    }
});

function addChat(chat_id) {
    let chat_name = 'Some chat';
    try {
        chat_name = $('#tg-chat-button-add' + chat_id).attr('tg-chat-name')
    } catch (e) {
        console.error(e)
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrf_token);
        }
    });
    $.post((document.location.origin + $('#tg-chat-search-form').attr('add-chat-action')), {
            chat_id: chat_id,
            chat_name: chat_name,
        },
        function (json) {
            if (json.result === true) {
                $(location).attr('href', json.redirect_to);
            }
            else {
                alert('Error while saving the chat')
            }
        }, 'json')
}


// $.getJSON($('#tg-form').attr('action'), {'phone': $('#tg-phone').attr('value')}, function (json) {
//     if (json.result === true) {
//         $(location).attr('href', json.redirect_to);
//     }
// })


// $(document).ready(function () {
//     $('input:radio[name=account]:checked').change(function () {
//         if ($("input[name='bedStatus']:checked").val() == 'allot') {
//             alert("Allot Thai Gayo Bhai");
//         }
//         if ($("input[name='bedStatus']:checked").val() == 'transfer') {
//             alert("Transfer Thai Gayo");
//         }
//     });
// });
//
//
// $('.card-input-element').click( function() {
//     let phone_number;
//     let contact_group_id;
//     let message_text;
//     try {
//         switch (this.name) {
//         case '‌account': phone_number = this.value;
//         break;
//         case '‌contact_group': contact_group_id = this.value;
//         break;
//         case 'message': message_text = this.value;
//         break
//     }
//     if (phone_number && contact_group_id && message_text) {
//         alert(phone_number + '\n' +
//               contact_group_id + '\n' +
//               message_text)
//     }
//     }
//     catch (e) {
//         console.error(e)
//     }
//
// });