from django import forms
from .models import CreatedGroup


class CreatedGroupForm(forms.ModelForm):
    class Meta:
        model = CreatedGroup
        exclude = ('users_to_add_count', 'users_added_count', 'chat_id')
        widgets = {
            'group_name': forms.TextInput(attrs={'class': 'form-control'}),
        }


class ImportContactForm(forms.Form):
    file = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control'}), required=False)

    def clean_file(self):
        file = self.cleaned_data['file']
        valid_extensions = ['csv', 'xlsx', 'ods']
        if file:
            extension = str(file.name.split('.')[-1]).lower()
            if extension not in valid_extensions:
                raise forms.ValidationError("The file does not match valid extensions "
                                            "('csv', 'xlsx', 'ods)")
            return file