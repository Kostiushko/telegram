# Generated by Django 2.1.7 on 2019-04-04 10:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0014_createdgroup_chat_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='createdgroup',
            name='chat_id',
            field=models.BigIntegerField(blank=True, null=True, unique=True),
        ),
    ]
