from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.app_home, name='App-home'),
    path('th', views.th_render, name='App-th'),
    path('accounts/add', views.add_account, name='Accounts-add'),
    path('accounts/delete/<int:phone_number>/', views.delete_account, name='Accounts-delete'),
    # path('accounts/manage', views.app_home, name='Accounts-manage'),
    path('contacts/add', views.add_contacts, name='Contacts-add'),
    path('contacts/add/<int:export>/', views.add_contacts, name='export'),
    path('contacts/delete/<int:chat_id>/', views.delete_contact_group, name='Contacts-delete'),
    path('contacts/manage/<int:chat_id>/', views.manage_contacts_group, name='Contacts-manage'),
    path('contacts/manage/<int:chat_id>/<int:to_csv>', views.manage_contacts_group, name='contact_to_csv'),
    # path('messages/add', views.add_message, name='Messages-add'),
    path('messages/delete/<str:message_name>', views.delete_message, name='Messages-delete'),
    re_path(r'^delete/(?P<group_id>[0-9]+)/$', views.delete_created_group, name="delete"),
    # path('messages/manage', views.app_home, name='Messages-manage'),
    path('api/account/add', views.api_account_add, name='Api-account-add'),
    # path('api/contacts/add', views.api_account_add, name='Api-account-add'),
    path('api/chat/add', views.api_add_chat, name='Api-add-chat'),
    path('api/chats/search', views.api_get_chats, name='Api-search-chats'),
    path('api/message/send', views.api_send_message, name='Api-send-message'),
    path('api/group/create', views.api_add_users_to_group, name='Api-create-group'),
    # path('export_to_csv/<int:to_csv>/', views.app_home, name="to_csv")
    re_path(r'^add_contacts_to/(?P<chat_id>[0-9]+)', views.add_users_to_group, name="add_contacts_to_group")
]
