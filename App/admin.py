from django.contrib import admin

from App.models import Message, Account, ContactGroup, CreatedGroup

admin.site.register(Message)
admin.site.register(Account)
admin.site.register(ContactGroup)
admin.site.register(CreatedGroup)
