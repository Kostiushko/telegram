from django.shortcuts import render


def home(request):
    return render(request, 'TelegramApplication/home.html', {'title': 'Home'})
