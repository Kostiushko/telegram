from django.apps import AppConfig


class TelegramapplicationConfig(AppConfig):
    name = 'TelegramApplication'
