import asyncio
import json
import logging
import os
import sqlite3
from random import randint

from telethon import TelegramClient
from telethon.errors import PeerFloodError, InputUserDeactivatedError
from telethon.sessions import StringSession
from telethon.tl.functions.account import CheckUsernameRequest
from telethon.tl.functions.channels import GetParticipantsRequest, InviteToChannelRequest
from telethon.tl.functions.contacts import ResolveUsernameRequest, SearchRequest
from telethon.tl.functions.messages import SearchGlobalRequest, GetFullChatRequest, \
  SendMessageRequest, CreateChatRequest, AddChatUserRequest, MigrateChatRequest  # , SearchRequest
from telethon.tl.functions.users import GetFullUserRequest
from telethon.tl.types import ChannelParticipantsSearch, TypeInputChannel, ChannelParticipantsAdmins, PeerChannel, \
  InputPeerUser, InputUser, ChannelParticipantAdmin, UserStatusOnline, PeerChat, ChatParticipantAdmin
from telethon import functions, types
from time import sleep
import datetime
from telethon.utils import parse_phone


# TODO: write this using asyncio
# TODO: What if FloodError or 2FA?


class Connector:
  def __init__(self, session: str = None,
               phone_number: str = None):
    # self.api_id = 264679
    # self.api_hash = '8cfdc194043267bb0193667e93842a46'
    self.api_id = 648432
    self.api_hash = 'c13cbe356553a61e1ecaf1ef02d6001c'
    # self.api_id = 502984
    # self.api_hash = 'b9968605c33e4e4d2132524af6de5dec'
    self.session = StringSession(session)
    # Phone number needs only for creating account  # TODO: fix this
    self.phone_number = parse_phone(phone_number)
    self.client = TelegramClient(self.session, self.api_id, self.api_hash,
                                 device_model='SimpleTelegramApplication',
                                 loop=asyncio.set_event_loop(asyncio.new_event_loop()))
    self.client.loop.run_until_complete(self.client.connect())

    # self.client.connect()

  async def start(self, phone_number: (str, int) = None) -> bool:
    if not await self.client.is_user_authorized():
      sent_code = await self.client.send_code_request(phone_number)
      return sent_code.phone_registered
    else:
      return True

  async def sign_in(self, code: str) -> bool:
    await self.client.sign_in(self.phone_number, code)
    return await self.client.is_user_authorized()

  async def get_first_name(self):
    return (await self.client.get_me()).username

  async def search_chats(self, query: str,
                         limit: int = 5) -> str:
    result = await self.client(SearchRequest(query, limit=limit))
    return result.to_json()

  # async def get_chat(self, chat_id: int):
  #     result = await self.client(GetFullChatRequest(chat_id))

  async def get_participants(self, chat_id: int,
                             chat_name: str,
                             # offset: int = 0,
                             # limit: int = 200,
                             # one_time_request: bool = False
                             ) -> tuple:
    # Search chats for adding entity to our list
    await self.client(SearchRequest(chat_name, limit=15))

    results = []
    result = await self.client.get_participants(PeerChannel(chat_id),
                                                filter=ChannelParticipantsSearch(''),
                                                aggressive=True)
    for user in result:
      if not type(user.participant) == ChannelParticipantAdmin:
        results.append(user.to_json())

    return json.dumps(results), len(result)

  async def get_participants_by_chat(self, chat_id: int,
                                     chat_name: str,
                                     # offset: int = 0,
                                     # limit: int = 200,
                                     # one_time_request: bool = False
                                     ) -> tuple:
    # Search chats for adding entity to our list
    await self.client(SearchRequest(chat_name, limit=15))

    results = []
    result = await self.client.get_participants(PeerChat(chat_id))
    for user in result:
      if not type(user.participant) == ChatParticipantAdmin:
        results.append(user.to_json())

    return json.dumps(results), len(result)

  async def send_messages(self, chat_name: str,
                          chat_id: str,  # TODO: is it always an integer-like?
                          # recipients: list,
                          message: str) -> bool:

    # Search chats for adding entity to our list
    await self.client(SearchRequest(chat_name, limit=5))

    # For storing it to entities or whatever
    result = await self.client.get_participants(PeerChannel(int(chat_id)),
                                                filter=ChannelParticipantsSearch(''),
                                                aggressive=True)
    # try:
    messages_sent_count = 0
    peer_flood_error_wait = 3600

    db = sqlite3.connect(os.path.join(os.path.dirname(__file__), 'backend_db'))
    c = db.cursor()

    self_telegram = await self.client.get_me()
    self_telegram_id = self_telegram.id

    # Save users to DB

    for user in result:
      if not type(user.participant) == ChannelParticipantAdmin:
        # Save only if we don't have this item in DB
        if len(c.execute(f"SELECT * FROM messages_sent WHERE to_telegram_user_id = {user.id} "
                         f"AND message_text = '{message}'").fetchall()) == 0:
          c.execute(f"INSERT INTO messages_sent VALUES ({self_telegram_id}, {user.id}, '{message}',"
                    f"'FALSE', 'NULL', 'FALSE', '{chat_name}', {chat_id})")
    #     parameters.append((f'{self_telegram_id}, {user.id}, {message}, '
    #                        f'{False}, {None}, {False}, {chat_name}, {chat_id}'))
    # c.executemany('INSERT INTO messages_sent VALUES (?, ?, ?, ?, ?, ?, ?, ?)', (parameters,))
    db.commit()

    # Get a user from DB and try to send a message
    while True:
      # try to get user for this TG account
      users_in_db_unhandled = c.execute("SELECT * FROM messages_sent WHERE is_sent = 'FALSE' "
                                        f"AND is_handled = 'FALSE'").fetchall()
      if len(users_in_db_unhandled) == 0:
        # TODO: make is_handled feature
        print('From this account Unhandled Sent successfully')
        db.close()
        return True
      # get a user
      user = users_in_db_unhandled[randint(0, len(users_in_db_unhandled) - 1)]
      # Set the user is handled
      c.execute(f"UPDATE messages_sent SET is_handled = 'TRUE', time_handled = CURRENT_TIME "
                f"WHERE to_telegram_user_id = {user[1]} AND message_text = '{message}'")
      db.commit()
      try:
        # Yeah, goddamn it, we need to get all entries again, so...
        # Search chats for adding entity to our list
        await self.client(SearchRequest(user[6], limit=5))

        # For storing it to entities or whatever
        result = await self.client.get_participants(PeerChannel(int(user[7])),
                                                    filter=ChannelParticipantsSearch(''),
                                                    aggressive=True)

        # tg_user = await self.client.get_input_entity(InputPeerUser(user['id'], user['access_hash']))
        # # entity = await self.client.get_entity(tg_user)
        # print(await self.client(SendMessageRequest(tg_user, message)))
        for tg_user in result:
          if tg_user.id == user[1]:
            if not tg_user.is_self and not tg_user.bot and \
                not type(user.participant) == ChannelParticipantAdmin:
              await self.client.send_message(user[1], message)
        c.execute(f"UPDATE messages_sent SET is_sent = 'TRUE' "
                  f"WHERE to_telegram_user_id = {user[1]} AND message_text = '{message}'")
        db.commit()
        messages_sent_count += 1
        peer_flood_error_wait = 360
        print(f'{messages_sent_count} messages sent!')

      except PeerFloodError:
        peer_flood_error_wait += 5
        logging.error(f'Peer flood error! Waiting {peer_flood_error_wait} seconds...')
        await asyncio.sleep(randint(20, peer_flood_error_wait))
      except ConnectionError:
        await self.client.connect()
      except InputUserDeactivatedError:
        logging.error(f'Deactivated user found!')  # TODO: add db.execute() for this
        await asyncio.sleep(5)
      except Exception as e:
        logging.error(e.__class__, e)
        await asyncio.sleep(randint(15, 20))
      if len(result) == 0:
        break
      await asyncio.sleep(randint(5, 20))
    return True
    # except Exception as e:
    #     # TODO: make logging here
    #     return False

  async def add_users_to_group(self,
                               chat_name: str,
                               chat_id: str,
                               group_name: str,
                               group_to_create_id: int) -> (bool, int):

    # Search chats for adding entity to our list
    await self.client(SearchRequest(chat_name, limit=5))

    # For storing it to entities or whatever
    result = await self.client.get_participants(PeerChannel(int(chat_id)),
                                                filter=ChannelParticipantsSearch(''),
                                                aggressive=True)

    db = sqlite3.connect(os.path.join(os.path.dirname(__file__), 'backend_db'))

    c = db.cursor()
    self_telegram = await self.client.get_me()
    self_telegram_id = self_telegram.id
    #
    # users = []
    # # users_text = ''
    # for user in result:
    #     # users_text += user.to_json() + '\n'
    #     if not user.is_self and not user.bot and \
    #             not type(user.participant) == ChannelParticipantAdmin and not user.deleted:
    #         users.append(user)
    # # with open('users_text.json', 'w') as file:
    # #     file.write(users_text)

    for user in result:
      if not user.is_self and not user.bot and \
          not type(user.participant) == ChannelParticipantAdmin and not user.deleted:
        # Save only if we don't have this item in DB
        if len(c.execute(f"SELECT * FROM users_for_groups WHERE user_id = {user.id} "
                         # f"AND to_group = '{group_to_create_id}'"
                         ).fetchall()) == 0:
          # # Is user online checking:
          # online = 'TRUE' if type(user.status) == UserStatusOnline else 'FALSE'
          c.execute(f"INSERT INTO users_for_groups VALUES ({user.id}, {group_to_create_id}, "
                    f"'{chat_name}', {chat_id}, "
                    f"'FALSE', 'NULL', {self_telegram_id}, 'FALSE', "
                    f"'{'TRUE' if type(user.status) == UserStatusOnline else 'FALSE'}')")
    db.commit()

    # users.append('TrDex')

    # Create channel
    # from telethon.tl.functions.channels import CreateChannelRequest #  CheckUsernameRequest, UpdateUsernameRequest
    # from telethon.tl.types import InputChannel, InputPeerChannel

    # if len(c.execute(f"SELECT * FROM created_groups"
    #                  f" WHERE group_id = {group_to_create_id}"
    #                  ).fetchall()) == 0:
    users = [item[0] for item in c.execute(f"SELECT user_id FROM users_for_groups "
                                           f"WHERE is_online = 'FALSE' "
                                           f"AND added = 'FALSE' "
                                           f"LIMIT 48").fetchall()]
    if len(users) < 48:
      logging.info('All offline users added!')

      to_add = 48 - len(users)
      users_to_add = [item[0] for item in c.execute(f"SELECT user_id FROM users_for_groups "
                                                    f"WHERE added = 'FALSE' "
                                                    f"LIMIT {to_add}").fetchall()]
      users.extend(users_to_add)

    if len(users) == 0:
      return True, len(users)

    tg_users = []
    for user in users:
      for tg_user in result:
        if int(user) == tg_user.id:
          if not type(tg_user.participant) == ChannelParticipantAdmin and not tg_user.deleted:
            tg_users.append(user)
            break
    await self.client(CreateChatRequest(users=tg_users + ['TrDex'], title=group_name))
    print(f'Group {group_name} created!')

    for user in users:
      c.execute(f"UPDATE users_for_groups SET added = 'TRUE' "
                f"WHERE user_id = {user}")
    db.commit()
    return True, len(users)

    # Convert the group to a supergroup
    # res = await self.client(MigrateChatRequest(updates.chats[0].id))
    # c.execute(f"INSERT INTO created_groups VALUES ({group_to_create_id}, '{group_name}', "
    #           f"CURRENT_TIME, {self_telegram_id}, {res.chats[1].id})")
    # db.commit()
    # print(res.stringify())
    # print(f'Supergroup {group_name} created!')
    # return True
    # else:
    #
    #     # # Search chats for adding entity to our list
    #     # await self.client(SearchRequest(chat_name, limit=5))
    #     #
    #     # # For storing it to entities or whatever
    #     # result = await self.client.get_participants(PeerChannel(int(chat_id)),
    #     #                                             filter=ChannelParticipantsSearch(''),
    #     #                                             aggressive=True)
    #
    #     while c.execute("SELECT COUNT(*) FROM users_for_groups WHERE added = 'FALSE'").fetchone()[0] >= 40:
    #         # db_users = c.execute(f"SELECT user_id, to_group FROM users_for_groups "
    #         #                      f"WHERE to_group = {group_to_create_id} AND is_online = 'FALSE'").fetchall()
    #         users = [item[0] for item in c.execute(f"SELECT user_id FROM users_for_groups "
    #                                                f"WHERE to_group = {group_to_create_id} "
    #                                                f"AND is_online = 'FALSE' LIMIT 40").fetchall()]
    #         tg_chat_id = [item[0] for item in c.execute(f"SELECT tg_chat_id FROM created_groups "
    #                                                     f"WHERE group_id = {group_to_create_id}").fetchall()][0]
    #
    #         users_added_count = 0
    #
    #         # user_ids = [int(user) for user in users]
    #         tg_users = []
    #         for user in users:
    #             for tg_user in result:
    #                 if int(user) == tg_user.id:
    #                     tg_users.append(tg_user)
    #
    #         try:
    #             await self.client(InviteToChannelRequest(int(tg_chat_id), [
    #                 InputPeerUser(user_id_new.username) for user_id_new in result[-40:]
    #             ]))
    #             for user in users:
    #                 c.execute(f"UPDATE users_for_groups SET added = 'TRUE' "
    #                           f"WHERE user_id = {user} AND to_group = '{group_to_create_id}'")
    #                 db.commit()
    #                 users_added_count += 1
    #                 print(f'User {user} added!')
    #                 print(f'Added {users_added_count} users at all!')
    #         except Exception as e:
    #             logging.error(e.__class__, e)
    #             await asyncio.sleep(randint(1, 3))
    #
    #     return True

    # await self.client(InviteToChannelRequest(createdPrivateChannel.chats[0], result))


# channel = TypeInputChannel()


# result = client(SearchGlobalRequest(
#     q='bitNet',
#     offset_date=None,
#     offset_peer=types.InputPeerEmpty(),
#     offset_id=0,
#     limit=10
# ))
# print(result.stringify())

# res = client(ResolveUsernameRequest('BitNews1'))
# channel_name = 'GitLabs'
# message = 'Sorry about this message and spam. I am just trying to create some Telegram apps and I needed to try to ' \
#           'send you some messages. Hope you will understand this'
# channels = client(SearchRequest(channel_name, limit=5))
#
# participants = client(functions.channels.GetParticipantsRequest(channels.chats[0].id,
#                                                                 filter=ChannelParticipantsSearch(''),
#                                                                 offset=0, limit=12, hash=0))
#
# for user in participants.users:
#     client.send_message(user.id, message)
# breakpoint()
#
#
#
#
#
# while True:
#     participants = client(GetParticipantsRequest(channel, ChannelParticipantsSearch(''), offset, limit, hash=0))
#     if not participants.users:
#         break
#     all_participants.extend(participants.users)
#     offset += len(participants.users)
# breakpoint()


if __name__ == '__main__':
  # For 0935531336
  session = '1BJWapzMBu5X1iQBTYEKqQ9lN1tsNzdWZ7YMXMM3qwiAX2jYv7s5W9Nq6ANn6eM8hOqH7nTzMJbrI2MYglXKnPRJp-DFhghSMltnqevex49GYk0XOX5T9RYHf6IwTvGMFHevogHicq0kZSMd9kx1hW9PlM2vJBXTcQOqsORWQWyXZklVWpbhYAZF_x5AlSISlF5kUbnycUkziMKDw_73wXCDXdHhLekqgVSIn0pSnzv9v9PUfGPyx1TLAKS4qX4ReoKNmyUlwvDcb8xxO1k5N1UNV2iMRq6DywUl5PhAP_WC6zcsf2Ukqs4W8MxrK7hnlz_WnYZjaLKXYQOiAyk_1Vu7E8P8toFU='
  c_ = Connector(session)
  # f = c.client.loop.run_until_complete(c.search_chats('bitmart'))
  y = c_.client.loop.run_until_complete(c_.add_users_to_group('bitmart', '1196969278', 'new group'))
  import json

  # f = json.loads(f)
  # h = f['chats'][0]['id']
  #
  # result_ = c.client.loop.run_until_complete(c.get_participants(h, 'bitmart'))
  # le = len(result_)
  #
  # message_ = 'Sorry about this message and spam. I am just trying to create some Telegram apps and I needed to try to ' \
  #           'send you some messages. Hope you will understand this'
  # phone_number_ = '+380935531336'
  # if c.start('+380935531336'):
  #     c.sign_in(phone_number_, input('Enter code'))
  # else:
  #     print('Input another phone number')

  # client = TelegramClient('anon', 264679, '8cfdc194043267bb0193667e93842a46')
  # assert client.connect()
  # if not client.is_user_authorized():
  #     client.send_code_request(phone_number)
  #     me = client.sign_in(phone_number, input('Enter code: '))

  # 1185569778
