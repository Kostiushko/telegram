from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.views import LoginView
from django.shortcuts import render, redirect
from django.contrib import messages
from django.views import View
from django.views.generic import FormView

from TelegramApp.settings import ACCESS_PIN_CODE


def login(request):
    if request.method == 'POST':
        # form = UserCreationForm(request.POST)
        if request.POST['pin-code'] and request.POST['pin-code'] == str(ACCESS_PIN_CODE):
            return redirect('App-home')
    return render(request, 'Users/login.html', {'title': 'Log In'})


class UserLoginView(LoginView):
    template_name = 'examples/500.html'

    def get(self, request, *args, **kwargs):
        return render(request, 'Users/login.html', {'title': 'Log In'})


@login_required
def mini(request):
    return render(request, 'Users/login.html')
